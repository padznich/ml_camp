from pathlib import Path

from utils.data_prepare import DataChef
from utils.csv_reader import read_csv
from utils.ml_algorithms.knn import KNN


PROJECT_ROOT = Path(Path().absolute())

data = read_csv(PROJECT_ROOT / 'data/crx.csv')
dc = DataChef(data)

dc.prepare_table()

dc.allocate_samples('A_15')

train_data = dict(
    X_train=dc.get_X_train(),
    X_test=dc.get_X_test(),
    y_train=dc.get_y_train(),
    y_test=dc.get_y_test(),
)


if __name__ == '__main__':

    knn = KNN(**train_data)

    y_train_predict, err_train, y_test_predict, err_test = knn.predict()
    print(err_train, err_test)

    y_train_predict, err_train, y_test_predict, err_test = knn.predict_with_grid()
    print(err_train, err_test)
