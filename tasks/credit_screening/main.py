from pathlib import Path

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from pandas.tools.plotting import scatter_matrix

from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn import ensemble


PROJECT_ROOT = Path(Path().absolute().as_posix() + '/../..').resolve()


path = Path(PROJECT_ROOT.as_posix() + '/data/crx.csv').resolve()

data = pd.read_csv(path, header=None, na_values='?')
data.columns = ['A' + str(i) for i in range(1, 16)] + ['class']

categorical_columns = [c for c in data.columns if data[c].dtype.name == 'object']
numerical_columns = [c for c in data.columns if data[c].dtype.name != 'object']

# print(data.tail())
# print(categorical_columns)
# print(numerical_columns)
# print(data[categorical_columns].describe())  # print(data.describe(include=[object]))

# # полный перечень значений категориальных признаков
# for c in categorical_columns:
#     print(data[c].unique())

# # построим для каждой количественной переменной гистограмму, а для каждой пары таких переменных – диаграмму рассеяния
# scatter_matrix(data, alpha=0.05, figsize=(10, 10))
# plt.style.use('ggplot')
# plt.show()
# # OR
# data.corr()


# # Можно выбрать любую пару признаков и нарисовать диаграмму рассеяния для этой пары признаков,
# # изображая точки, соответствующие объектам из разных классов разным цветом: + – красный, - – синий
#
# col1 = 'A2'
# col2 = 'A11'
#
# plt.figure(figsize=(10, 6))
#
# plt.scatter(data[col1][data['class'] == '+'],
#             data[col2][data['class'] == '+'],
#             alpha=0.75,
#             color='red',
#             label='+')
#
# plt.scatter(data[col1][data['class'] == '-'],
#             data[col2][data['class'] == '-'],
#             alpha=0.75,
#             color='blue',
#             label='-')
#
# plt.xlabel(col1)
# plt.ylabel(col2)
# plt.legend(loc='best')
# plt.show()
#
# # Из диаграммы, в частности, видно, что признак A11 является существенным:
# # как правило, красные точки имеют большое значение этого признака, а синие – маленькое.
# # Иными словами, визуально наблюдается хорошая корреляция между признаками A11 и class.
# # Признак A2, напротив, несет гораздо меньше информации о принадлежности объекта интересующему нас классу.

#
# библиотека scikit-learn не умеет напрямую обрабатывать категориальные признаки.
#

# # Узнать количество заполненных (непропущенных) элементов можно с помощью метода count
# print(data.count(axis=0))  # by column
# print(data.count(axis=1))  # by row
# # Если данные содержат пропущенные значения, то имеется две простые альтернативы:
# #   удалить столбцы с такими значениями (data = data.dropna(axis=1)),
# #   удалить строки с такими значениями (data = data.dropna(axis=0)).
# #
# # # Количественные признаки
# # Заполнить пропущенные значения можно с помощью метода fillna. Заполним, например, медианными значениями.
# # axis=0 по-прежнему указывает, что мы двигаемся сверху вниз:
data = data.fillna(data.median(axis=0), axis=0)

# # # Категориальные признаки
# # Теперь рассмотрим пропущенные значения в столбцах, соответствующих категориальным признакам.
# #  Простая стратегия – заполнение пропущенных значений самым популярным в столбце. Начнем с A1:
# data['A1'].describe()
# data['A1'] = data['A1'].fillna('b')
# # Автоматизируем процесс:
data_describe = data.describe(include=[object])
for c in categorical_columns:
    data[c] = data[c].fillna(data_describe[c]['top'])


# # Категориальные признаки, принимающие два значения (т.е. бинарные признаки)
# # и принимающие большее количество значений будем обрабатывать по-разному.
# #
# # Вначале выделим бинарные и небинарные признаки:

binary_columns = [c for c in categorical_columns if data_describe[c]['unique'] == 2]
nonbinary_columns = [c for c in categorical_columns if data_describe[c]['unique'] > 2]

for c in binary_columns:
    top = data_describe[c]['top']
    top_items = data[c] == top
    data.loc[top_items, c] = 0
    data.loc[np.logical_not(top_items), c] = 1

# # Небинарные признаки
# print(data['A4'].unique())  # ['u' 'y' 'l']
# # Заменим признак A4 тремя признаками: A4_u, A4_y, A4_l.
# #     Если признак A4 принимает значение u, то признак A4_u равен 1, A4_y равен 0, A4_l равен 0.
# #     Если признак A4 принимает значение y, то признак A4_y равен 0, A4_y равен 1, A4_l равен 0.
# #     Если признак A4 принимает значение l, то признак A4_l равен 0, A4_y равен 0, A4_l равен 1.
data_nonbinary = pd.get_dummies(data[nonbinary_columns])
# print(data.head())


#
# # Нормализация количественных признаков
# Многие алгоритмы машинного обучения чувствительны к масштабированию данных.
# К таким алгоритмам, например, относится метод ближайших соседей, машина опорных векторов и др.
#
# В этом случае количественные признаки полезно нормализовать.
# Это можно делать разными способами.
# Например, каждый количественный признак приведем к нулевому среднему и единичному среднеквадратичному отклонению:
data_numerical = data[numerical_columns]
data_numerical = (data_numerical - data_numerical.mean()) / data_numerical.std()
# print(data_numerical.describe())
# print(data_numerical.head())

# Соединим все столбцы в одну таблицу:
data = pd.concat((data_numerical, data[binary_columns], data_nonbinary), axis=1)
data = pd.DataFrame(data, dtype=float)
# print(data_numerical.head())

# Для удобства отдельно рассмотрим столбцы, соответствующие входным признакам (это будет матрица X),
# а отдельно – выделенный признак (вектор y):
X = data.drop('class', axis=1)  # Выбрасываем столбец 'class'.
y = data['class']
feature_names = X.columns
# print(feature_names)
N, d = X.shape
# print(N, d)


#
# Обучающая и тестовая выборки
#
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=11)

# N_train, _ = X_train.shape
# N_test,  _ = X_test.shape
# print(N_train, N_test)


# #
# # kNN
# #
# knn = KNeighborsClassifier()
# knn.fit(X_train, y_train)
#
# y_train_predict = knn.predict(X_train)
# y_test_predict = knn.predict(X_test)
#
# err_train = np.mean(y_train != y_train_predict)
# err_test = np.mean(y_test  != y_test_predict)
# print('\nKNeighborsClassifier')
# print(err_train, err_test)
#
# # GridSearchCV
# n_neighbors_array = [1, 3, 5, 7, 10, 15]
# knn = KNeighborsClassifier()
# grid = GridSearchCV(knn, param_grid={'n_neighbors': n_neighbors_array}, cv=5)
# grid.fit(X_train, y_train)
#
# best_cv_err = 1 - grid.best_score_
# best_n_neighbors = grid.best_estimator_.n_neighbors
# print('\nKNeighborsClassifier GridSearchCV')
# print(best_cv_err, best_n_neighbors)
#
#
# knn = KNeighborsClassifier(n_neighbors=best_n_neighbors)
# knn.fit(X_train, y_train)
#
# err_train = np.mean(y_train != knn.predict(X_train))
# err_test  = np.mean(y_test  != knn.predict(X_test))
# print('\nKNeighborsClassifier GridSearchCV with best_n_neighbors')
# print(err_train, err_test)

# #
# # SVC / SVM
# #
# svc = SVC()
# svc.fit(X_train, y_train)
#
# err_train = np.mean(y_train != svc.predict(X_train))
# err_test = np.mean(y_test != svc.predict(X_test))
# print(err_train, err_test)
#
# # Radial Core
# C_array = np.logspace(-3, 3, num=7)
# gamma_array = np.logspace(-5, 2, num=8)
# svc = SVC(kernel='rbf')
# grid = GridSearchCV(svc, param_grid={'C': C_array, 'gamma': gamma_array})
# grid.fit(X_train, y_train)
# print('CV error    = ', 1 - grid.best_score_)
# print('best C      = ', grid.best_estimator_.C)
# print('best gamma  = ', grid.best_estimator_.gamma)
#
#
# svc = SVC(kernel='rbf', C=grid.best_estimator_.C, gamma=grid.best_estimator_.gamma)
# svc.fit(X_train, y_train)
#
# err_train = np.mean(y_train != svc.predict(X_train))
# err_test = np.mean(y_test != svc.predict(X_test))
# print(err_train, err_test)
#
#
# # Linear Core
# C_array = np.logspace(-3, 3, num=7)
# svc = SVC(kernel='linear')
# grid = GridSearchCV(svc, param_grid={'C': C_array})
# grid.fit(X_train, y_train)
# print('CV error    = ', 1 - grid.best_score_)
# print('best C      = ', grid.best_estimator_.C)
#
# svc = SVC(kernel='linear', C=grid.best_estimator_.C)
# svc.fit(X_train, y_train)
#
# err_train = np.mean(y_train != svc.predict(X_train))
# err_test = np.mean(y_test != svc.predict(X_test))
# print(err_train, err_test)
#
#
# # Polinominal Core
# C_array = np.logspace(-5, 2, num=8)
# gamma_array = np.logspace(-5, 2, num=8)
# degree_array = [2, 3, 4]
# svc = SVC(kernel='poly')
# grid = GridSearchCV(svc, param_grid={'C': C_array, 'gamma': gamma_array, 'degree': degree_array})
# grid.fit(X_train, y_train)
# print('CV error    = ', 1 - grid.best_score_)
# print('best C      = ', grid.best_estimator_.C)
# print('best gamma  = ', grid.best_estimator_.gamma)
# print('best degree = ', grid.best_estimator_.degree)
#
# svc = SVC(kernel='poly', C=grid.best_estimator_.C,
#           gamma=grid.best_estimator_.gamma, degree=grid.best_estimator_.degree)
# svc.fit(X_train, y_train)
#
# err_train = np.mean(y_train != svc.predict(X_train))
# err_test = np.mean(y_test != svc.predict(X_test))
# print(err_train, err_test)


# #
# # Random Forest
# #
# rf = ensemble.RandomForestClassifier(n_estimators=100, random_state=11)
# rf.fit(X_train, y_train)
#
# err_train = np.mean(y_train != rf.predict(X_train))
# err_test = np.mean(y_test != rf.predict(X_test))
# print(err_train, err_test)
#
# importances = rf.feature_importances_
# indices = np.argsort(importances)[::-1]
#
# print("Feature importances:")
# for f, idx in enumerate(indices):
#     print("{:2d}. feature '{:5s}' ({:.4f})".format(f + 1, feature_names[idx], importances[idx]))
#
# d_first = 20
# plt.figure(figsize=(8, 8))
# plt.title("Feature importances")
# plt.bar(range(d_first), importances[indices[:d_first]], align='center')
# plt.xticks(range(d_first), np.array(feature_names)[indices[:d_first]], rotation=90)
# plt.xlim([-1, d_first])
# plt.show()
# best_features = indices[:8]
# best_features_names = feature_names[best_features]
# print(best_features_names)


# #
# # GBT
# #
# gbt = ensemble.GradientBoostingClassifier(n_estimators=100, random_state=11)
# gbt.fit(X_train, y_train)
#
# # importances
# importances = gbt.feature_importances_
# indices = np.argsort(importances)[::-1]
# best_features = indices[:8]
# best_features_names = feature_names[best_features]
#
# err_train = np.mean(y_train != gbt.predict(X_train))
# err_test = np.mean(y_test != gbt.predict(X_test))
# print(err_train, err_test)
#
# gbt = ensemble.GradientBoostingClassifier(n_estimators=100, random_state=11)
#
# gbt.fit(X_train[best_features_names], y_train)
#
# err_train = np.mean(y_train != gbt.predict(X_train[best_features_names]))
# err_test = np.mean(y_test != gbt.predict(X_test[best_features_names]))
# print(err_train, err_test)
