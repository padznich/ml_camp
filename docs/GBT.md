Gradient Boosting Tree
=

На каждой итерации строится новый классификатор, аппроксимирующий значение градиента функции потерь.

```python
gbt = ensemble.GradientBoostingClassifier(n_estimators=100, random_state=11)
gbt.fit(X_train, y_train)

err_train = np.mean(y_train != gbt.predict(X_train))
err_test = np.mean(y_test != gbt.predict(X_test))
print(err_train, err_test)
```
Ошибка на тестовой выборке **10.1%** чуть выше, чем для случайного леса.



Заметим, что при использовании найденных выше значимых признаков ошибка практически не меняется:
```python
gbt = ensemble.GradientBoostingClassifier(n_estimators=100, random_state=11)
gbt.fit(X_train, y_train)

# importances
importances = gbt.feature_importances_
indices = np.argsort(importances)[::-1]
best_features = indices[:8]
best_features_names = feature_names[best_features]

gbt = ensemble.GradientBoostingClassifier(n_estimators=100, random_state=11)

gbt.fit(X_train[best_features_names], y_train)

err_train = np.mean(y_train != gbt.predict(X_train[best_features_names]))
err_test = np.mean(y_test != gbt.predict(X_test[best_features_names]))
print(err_train, err_test)
```
