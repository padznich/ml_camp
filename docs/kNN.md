kNN – метод ближайших соседей
=

Для нового объекта алгоритм ищет в обучающей выборке k наиболее близких объекта и относит новый объект к тому классу, которому принадлежит большинство из них.

___

Вначале обучим модель:
```python
from sklearn.neighbors import KNeighborsClassifier

knn = KNeighborsClassifier()
knn.fit(X_train, y_train)
```

___

После того, как модель обучена, мы можем предсказывать значение целевого признака по входным признакам для новых объектов.
Делается это с помощью метода **predict**

```python
y_train_predict = knn.predict(X_train)
y_test_predict = knn.predict(X_test)

err_train = np.mean(y_train != y_train_predict)
err_test  = np.mean(y_test  != y_test_predict)
print(err_train, err_test)
```

**err_train** и **err_test** – это ошибки на обучающей и тестовой выборках. Как мы видим, они составили 14.7% и 16.9%.

Для нас более важным является ошибка на тестовой выборке,
так как мы должны уметь предсказывать правильное (по возможности) значение на новых объектах,
которые при обучении были недоступны.

____

Попробуем уменьшить тестовую ошибку, варьируя параметры метода.

Основной параметр метода k-ближайших соседей – это **k**

Поиск оптимальных значений параметров можно осуществить с помощью класса GridSearchCV – поиск наилучшего набора параметров, доставляющих минимум ошибке перекрестного контроля (cross-validation).
По умолчанию рассматривается 3-кратный перекрестный контроль.

Например, найдем наилучшее значение k среди значений [1, 3, 5, 7, 10, 15]:
```python
from sklearn.grid_search import GridSearchCV
n_neighbors_array = [1, 3, 5, 7, 10, 15]
knn = KNeighborsClassifier()
grid = GridSearchCV(knn, param_grid={'n_neighbors': n_neighbors_array})
grid.fit(X_train, y_train)

best_cv_err = 1 - grid.best_score_
best_n_neighbors = grid.best_estimator_.n_neighbors
print(best_cv_err, best_n_neighbors)
```
В качестве оптимального метод выбрал значение k

равное 7. Ошибка перекрестного контроля составила 20.7%, что даже больше ошибки на тестовой выборке для 5 ближайших соседей. Это может быть обусленно тем, что для построения моделей в рамках схемы перекрестного контроля используются не все данные.

Проверим, чему равны ошибки на обучающей и тестовой выборках при этом значении параметра

```python
knn = KNeighborsClassifier(n_neighbors=best_n_neighbors)
knn.fit(X_train, y_train)

err_train = np.mean(y_train != knn.predict(X_train))
err_test  = np.mean(y_test  != knn.predict(X_test))
print(err_train, err_test)
```
Как видим, метод ближайших соседей на этой задаче дает не слишком удовлетворительные результаты.

