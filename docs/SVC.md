
SVC – машина опорных векторов
---

Следующий метод, который мы попробуем – машина опорных векторов (SVM – support vector machine или SVC – support vector classifier) – сразу приводит к более оптимистичным результатам.

Уже со значением параметров по умолчанию (в частности, ядро – радиальное rbf) получаем более низкую ошибку на обучающей выборке:

```python
from sklearn.svm import SVC
svc = SVC()
svc.fit(X_train, y_train)

err_train = np.mean(y_train != svc.predict(X_train))
err_test  = np.mean(y_test  != svc.predict(X_test))
print(err_train, err_test)
```
---

Итак, на тестовой выборке получили ошибку в **13%**.

С помощью подбора параметров попробуем ее еще уменьшить.

Радиальное ядро
=

Вначале попробуем найти лучшие значения параметров для радиального ядра.

```python
C_array = np.logspace(-3, 3, num=7)
gamma_array = np.logspace(-5, 2, num=8)
svc = SVC(kernel='rbf')
grid = GridSearchCV(svc, param_grid={'C': C_array, 'gamma': gamma_array})
grid.fit(X_train, y_train)
print('CV error    = ', 1 - grid.best_score_)
print('best C      = ', grid.best_estimator_.C)
print('best gamma  = ', grid.best_estimator_.gamma)
```

Посмотрим, чему равна ошибка на тестовой выборке при найденных значениях параметров алгоритма:
```python
svc = SVC(kernel='rbf', C=grid.best_estimator_.C, gamma=grid.best_estimator_.gamma)
svc.fit(X_train, y_train)

err_train = np.mean(y_train != svc.predict(X_train))
err_test  = np.mean(y_test  != svc.predict(X_test))
print err_train, err_test
```
Ошибка на тестовой выборке равна **11.1%**. Заметно лучше, чем kNN!


Линейное ядро
=
```python
C_array = np.logspace(-3, 3, num=7)
svc = SVC(kernel='linear')
grid = GridSearchCV(svc, param_grid={'C': C_array})
grid.fit(X_train, y_train)
print 'CV error    = ', 1 - grid.best_score_
print 'best C      = ', grid.best_estimator_.C
```

Получили ошибку перекрестного контроля в **15.1%**.
Посмотрим, чему равна ошибка на тестовой выборке при найденных значениях параметров алгоритма:
```python
svc = SVC(kernel='linear', C=grid.best_estimator_.C)
svc.fit(X_train, y_train)

err_train = np.mean(y_train != svc.predict(X_train))
err_test  = np.mean(y_test  != svc.predict(X_test))
print(err_train, err_test)
```
Ошибка на тестовой выборке составила **12.6%**.


Полиномиальное ядро
=
```python
C_array = np.logspace(-5, 2, num=8)
gamma_array = np.logspace(-5, 2, num=8)
degree_array = [2, 3, 4]
svc = SVC(kernel='poly')
grid = GridSearchCV(svc, param_grid={'C': C_array, 'gamma': gamma_array, 'degree': degree_array})
grid.fit(X_train, y_train)
print('CV error    = ', 1 - grid.best_score_)
print('best C      = ', grid.best_estimator_.C)
print('best gamma  = ', grid.best_estimator_.gamma)
print('best degree = ', grid.best_estimator_.degree)
```
Получили ошибку перекрестного контроля в **13.9%**.

Посмотрим, чему равна ошибка на тестовой выборке при найденных значениях параметров алгоритма:
```python
svc = SVC(kernel='poly', C=grid.best_estimator_.C, 
          gamma=grid.best_estimator_.gamma, degree=grid.best_estimator_.degree)
svc.fit(X_train, y_train)

err_train = np.mean(y_train != svc.predict(X_train))
err_test  = np.mean(y_test  != svc.predict(X_test))
print(err_train, err_test)
```
Ошибка на тестовой выборке составила **12.1%**.

