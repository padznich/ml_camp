from pathlib import Path
from logging import Logger

import pandas as pd


logger = Logger(__name__)


def read_csv(path: str, header: bool=None, na_values: str='?') -> pd.DataFrame:
    _path = Path(path)

    if not _path.exists():
        logger.warning('{} DOES NOT EXISTS'.format(path))
        return pd.DataFrame()

    return pd.read_csv(_path.resolve(), header=header, na_values=na_values)
