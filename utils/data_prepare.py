"""
Библиотека scikit-learn не умеет напрямую обрабатывать категориальные признаки.
______________________________________________________________________________

Узнать количество заполненных (непропущенных) элементов можно с помощью метода count
    print(data.count(axis=0))  # by column
    print(data.count(axis=1))  # by row
______________________________________________________________________________

Если данные содержат пропущенные значения, то имеется две простые альтернативы:
  удалить столбцы с такими значениями (data = data.dropna(axis=1)),
  удалить строки с такими значениями (data = data.dropna(axis=0)).
______________________________________________________________________________

Количественные признаки
Заполнить пропущенные значения можно с помощью метода fillna.
______________________________________________________________________________

Категориальные признаки
Теперь рассмотрим пропущенные значения в столбцах, соответствующих категориальным признакам.
 Простая стратегия – заполнение пропущенных значений самым популярным в столбце. Начнем с A1:
    data['A1'].describe()
    data['A1'] = data['A1'].fillna('b')
______________________________________________________________________________

Небинарные признаки
print(data['A4'].unique())  # ['u' 'y' 'l']
Заменим признак A4 тремя признаками: A4_u, A4_y, A4_l.
    Если признак A4 принимает значение u, то признак A4_u равен 1, A4_y равен 0, A4_l равен 0.
    Если признак A4 принимает значение y, то признак A4_y равен 0, A4_y равен 1, A4_l равен 0.
    Если признак A4 принимает значение l, то признак A4_l равен 0, A4_y равен 0, A4_l равен 1.
______________________________________________________________________________

Нормализация количественных признаков
Многие алгоритмы машинного обучения чувствительны к масштабированию данных.
К таким алгоритмам, например, относится метод ближайших соседей, машина опорных векторов и др.
В этом случае количественные признаки полезно нормализовать.
______________________________________________________________________________

"""
from logging import Logger

import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split


logger = Logger(__name__)


def get_binary_columns(data):
    categorical_data = data.describe(include=[object])
    categorical_columns = [column_name for column_name in categorical_data.columns]

    return [c for c in categorical_columns if categorical_data[c]['unique'] == 2]


def get_non_binary_columns(data):
    categorical_data = data.describe(include=[object])
    categorical_columns = [column_name for column_name in categorical_data.columns]

    return [c for c in categorical_columns if categorical_data[c]['unique'] > 2]


def fill_numeric_median(data: pd.DataFrame) -> pd.DataFrame:
    return data.fillna(data.median(axis=0), axis=0)


def fill_categorical_most_popular(data: pd.DataFrame) -> pd.DataFrame:

    out = data.copy()  # for pure function

    categorical_data = data.describe(include=[object])
    categorical_columns = [column_name for column_name in categorical_data.columns]

    for c in categorical_columns:
        out[c] = data[c].fillna(categorical_data[c]['top'])

    return out


def normalize_numerical(data):
    data_numerical = data[get_binary_columns(data)]
    return (data_numerical - data_numerical.mean()) / data_numerical.std()


def normalize_categorical_binary(data):
    out = data.copy()  # for pure function

    categorical_data = out.describe(include=[object])
    categorical_columns = [column_name for column_name in categorical_data.columns]
    binary_columns = [c for c in categorical_columns if categorical_data[c]['unique'] == 2]

    for c in binary_columns:
        top = categorical_data[c]['top']
        top_items = out[c] == top
        out.loc[top_items, c] = 0
        out.loc[np.logical_not(top_items), c] = 1
    return out


def main(data):
    data_numerical = normalize_numerical(data)
    binary_columns = get_binary_columns(data)
    non_binary_columns = get_non_binary_columns(data)
    data_non_binary = pd.get_dummies(data[non_binary_columns])

    data = pd.concat((data_numerical, data[binary_columns], data_non_binary), axis=1)
    return pd.DataFrame(data, dtype=float)


class DataChef:

    def __init__(self, data: pd.DataFrame):
        self.__data = data
        # Own column names
        self.__data.columns = ['A_' + str(i) for i in range(self.__data.shape[1])]

        self.__table = None
        self.__X = None  # Inputs
        self.__y = None  # Answer
        self.__X_train = None
        self.__X_test = None
        self.__y_train = None
        self.__y_test = None

        self.__numerical_description = data.describe(exclude=[object])  # type: pd.DataFrame
        self.__numerical_columns = [column for column in self.__numerical_description.columns]  # type: list[str]
        self.__numerical_data = self.__data[self.__numerical_columns]  # type: pd.DataFrame

        self.__categorical_description = data.describe(include=[object])  # type: pd.DataFrame
        self.__categorical_columns = [column for column in self.__categorical_description.columns]  # type: list[str]
        self.__categorical_data = self.__data[self.__categorical_columns]  # type: pd.DataFrame

        self.__binary_columns, self.__non_binary_columns = self.split_categorical_columns()

    def get_data(self):
        return self.__data

    def get_table(self) -> pd.DataFrame:
        return self.__table if self.__table is not None else self.prepare_table()

    def get_X(self):
        return self.__X

    def get_X_train(self):
        return self.__X_train

    def get_X_test(self):
        return self.__X_test

    def get_y(self):
        return self.__y

    def get_y_train(self):
        return self.__y_train

    def get_y_test(self):
        return self.__y_test

    def get_categorical_columns(self):
        return self.__categorical_columns

    def get_categorical_description(self):
        return self.__categorical_description

    def get_categorical_data(self):
        return self.__categorical_data

    def get_numerical_columns(self):
        return self.__numerical_columns

    def get_numerical_description(self):
        return self.__numerical_description

    def get_numerical_data(self):
        return self.__numerical_data

    def fill_numeric_with_median(self):
        self.__data = self.__data.fillna(self.__data.median(axis=0), axis=0)

    def fill_categorical_with_most_popular(self):
        for c in self.__categorical_columns:
            self.__data[c] = self.__data[c].fillna(self.__categorical_description[c]['top'])

    def get_normalized_numerical_data(self):
        return (self.__numerical_data - self.__numerical_data.mean()) / self.__numerical_data.std()

    def normalize_categorical_binary_data(self):
        for c in self.__binary_columns:
            top = self.__categorical_description[c]['top']
            top_items = self.__data[c] == top
            self.__data.loc[top_items, c] = 0
            self.__data.loc[np.logical_not(top_items), c] = 1

    def get_binary_data(self):
        self.normalize_categorical_binary_data()
        return self.__data[self.__binary_columns]

    def get_non_binary_data(self):
        return pd.get_dummies(self.__data[self.__non_binary_columns])

    def prepare_table(self) -> pd.DataFrame:
        """
        1. Save data to self.__table
        2. Return self.__table
        """
        # Fill table
        self.fill_numeric_with_median()
        self.fill_categorical_with_most_popular()

        # Fetch data
        data_numerical = self.get_normalized_numerical_data()
        data_binary = self.get_binary_data()
        data_non_binary = self.get_non_binary_data()

        # Compose Table
        data = pd.concat((data_numerical, data_binary, data_non_binary), axis=1)

        # Save table
        self.__table = pd.DataFrame(data, dtype=float)

        return self.__table

    def split_categorical_columns(self) -> tuple:
        """
        Split Categorical columns to Binary and Non Binary
        """
        binary_columns, non_binary_columns = [], []

        for c in self.__categorical_columns:
            if self.__categorical_description[c]['unique'] == 2:
                binary_columns.append(c)
            elif self.__categorical_description[c]['unique'] > 2:
                non_binary_columns.append(c)
            else:
                logger.warning('DataChef class self.__categorical_data[c]["unique"] < 2')
        return binary_columns, non_binary_columns

    def allocate_inputs_and_answer(self, feature_name: str):
        self.__X = self.__table.drop(feature_name, axis=1)
        self.__y = self.__table[feature_name]

    def allocate_samples(self, feature_name: str, test_size=0.3, random_state=11):
        self.allocate_inputs_and_answer(feature_name)
        results = train_test_split(self.__X, self.__y, test_size=test_size, random_state=random_state)
        self.__X_train, self.__X_test, self.__y_train, self.__y_test = [i.fillna(0.0) for i in results]


