"""
kNN - k-nearest neighbours
"""
import numpy as np
import pandas as pd

from sklearn.model_selection import  GridSearchCV
from sklearn.neighbors import KNeighborsClassifier


#
# kNN
#
class KNN:

    def __init__(self, X_train: pd.DataFrame, X_test: pd.DataFrame, y_train: pd.Series, y_test: pd.Series,
                 n_neighbors_array: list=None):
        self.__knn = KNeighborsClassifier()
        self.__X_train = X_train
        self.__X_test = X_test
        self.__y_train = y_train
        self.__y_test = y_test
        self.__n_neighbors_array = n_neighbors_array if n_neighbors_array else [1, 3, 5, 7, 10, 15]

    def predict(self) -> ((pd.Series, float), (pd.Series, float)):
        self.__knn.fit(self.__X_train, self.__y_train)

        # Prediction
        y_train_predict = self.__knn.predict(self.__X_train)
        y_test_predict = self.__knn.predict(self.__X_test)

        # Errors
        err_train = np.mean(self.__y_train != y_train_predict)
        err_test = np.mean(self.__y_test != y_test_predict)

        return y_train_predict, err_train, y_test_predict, err_test

    def predict_with_grid(self):
        self.__knn = KNeighborsClassifier(n_neighbors=self.get_best_n_neighbors())
        return self.predict()

    def get_best_n_neighbors(self):
        grid = GridSearchCV(self.__knn, param_grid={'n_neighbors': self.__n_neighbors_array}, cv=5)

        grid.fit(self.__X_train, self.__y_train)

        best_n_neighbors = grid.best_estimator_.n_neighbors

        # print('\nKNeighborsClassifier GridSearchCV')
        # print(1 - grid.best_score_, best_n_neighbors)

        return best_n_neighbors
